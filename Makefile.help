NAME
    hive_installer

SYNOPSIS
    make install
    make deploy init
    make deploy init dry
    make deploy (not finished)
    make deploy dry (not finished)

DESCRIPTION
    The Installer installs TYPO3 with composer.
    In addition, a dummy database is imported and the default extentsions are installed.

    Note: A configuration via browser is necessary for the installation.

REQUIREMENTS
    OS X 10.10.3 Yosemite or newer
    At least 4GB of RAM
    VirtualBox prior to version 4.3.30 must NOT be installed (it is incompatible with Docker for Mac). Docker for Mac will error out on install in this case. Uninstall the older version of VirtualBox and re-try the install.
    Git
    Composer
    Docker for Mac
    SourceTree
    SSH key authorisation for our servers
    hive_creator
    hive_docker

    See also: https://bitbucket.org/account/user/teufels/projects/DOC.

    Using the make command requires configuration of the corresponding 'install.ini' and in the 'hive_docker' folder must be execute 'make docker plain' before.

    See also: https://bitbucket.org/teufels/hive_installer
    See also: https://bitbucket.org/teufels/hive_creator

INSTRUCTIONS

    - Installing TYPO3

    Note:
    - Docker must be started.
    - development.localhost must be set in your host file.

    # Switch to the hive_install folder
    $ cd ../hive_install

    # Replace the missing placeholders from the install.ini
    $ nano install.ini # oder mit einem Editor bearbeiten

    # Add changed files
    $ git add install.ini

    # Commit changed files
    $ git commit -m 'modified install.ini'

    # Push changed files
    $ git push [alias] [branch]

    # Start installation
    $ make install

    After a while, the script invokes the Install tool in the browser. Please go through this installation manually. Then, by clicking on any key, continue the script.

    See also: https://bitbucket.org/teufels/documentation/wiki/TYPO3%20Installation

ERRORCODES:
    TODO: Helpful description of errorcodes and possible solutions.
    [g3978p78jzk312a0el4v1nd4ut6tb3z8]
    [dz67w45uqa1562czv25423r46w99k2te]
    [fsf9qxy3i3pi4k4usi18umx21r9q837v]

LISENCE:
    The MIT License (MIT)

    Copyright (c) 2016
    Andreas Hafner <a.hafner@teufels.com>,
    Dominik Hilser <d.hilser@teufels.com>,
    Georg Kathan <g.kathan@teufels.com>,
    Hendrik Krüger <h.krueger@teufels.com>,
    Perrin Ennen <p.ennen@teufels.com>,
    Timo Bittner <t.bittner@teufels.com>,
    teufels GmbH <digital@teufels.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
