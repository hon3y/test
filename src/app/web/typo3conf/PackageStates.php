<?php
# PackageStates.php

# This file is maintained by TYPO3's package management. Although you can edit it
# manually, you should rather use the extension manager for maintaining packages.
# This file will be regenerated automatically if it doesn't exist. Deleting this file
# should, however, never become necessary if you use the package commands.

return [
    'packages' => [
        'core' => [
            'packagePath' => 'typo3/sysext/core/',
        ],
        'extbase' => [
            'packagePath' => 'typo3/sysext/extbase/',
        ],
        'fluid' => [
            'packagePath' => 'typo3/sysext/fluid/',
        ],
        'frontend' => [
            'packagePath' => 'typo3/sysext/frontend/',
        ],
        'fluid_styled_content' => [
            'packagePath' => 'typo3/sysext/fluid_styled_content/',
        ],
        'info' => [
            'packagePath' => 'typo3/sysext/info/',
        ],
        'info_pagetsconfig' => [
            'packagePath' => 'typo3/sysext/info_pagetsconfig/',
        ],
        'extensionmanager' => [
            'packagePath' => 'typo3/sysext/extensionmanager/',
        ],
        'lang' => [
            'packagePath' => 'typo3/sysext/lang/',
        ],
        'setup' => [
            'packagePath' => 'typo3/sysext/setup/',
        ],
        'rte_ckeditor' => [
            'packagePath' => 'typo3/sysext/rte_ckeditor/',
        ],
        'rsaauth' => [
            'packagePath' => 'typo3/sysext/rsaauth/',
        ],
        'saltedpasswords' => [
            'packagePath' => 'typo3/sysext/saltedpasswords/',
        ],
        'func' => [
            'packagePath' => 'typo3/sysext/func/',
        ],
        'wizard_crpages' => [
            'packagePath' => 'typo3/sysext/wizard_crpages/',
        ],
        'wizard_sortpages' => [
            'packagePath' => 'typo3/sysext/wizard_sortpages/',
        ],
        'about' => [
            'packagePath' => 'typo3/sysext/about/',
        ],
        'backend' => [
            'packagePath' => 'typo3/sysext/backend/',
        ],
        'belog' => [
            'packagePath' => 'typo3/sysext/belog/',
        ],
        'beuser' => [
            'packagePath' => 'typo3/sysext/beuser/',
        ],
        'context_help' => [
            'packagePath' => 'typo3/sysext/context_help/',
        ],
        'cshmanual' => [
            'packagePath' => 'typo3/sysext/cshmanual/',
        ],
        'documentation' => [
            'packagePath' => 'typo3/sysext/documentation/',
        ],
        'felogin' => [
            'packagePath' => 'typo3/sysext/felogin/',
        ],
        'filelist' => [
            'packagePath' => 'typo3/sysext/filelist/',
        ],
        'filemetadata' => [
            'packagePath' => 'typo3/sysext/filemetadata/',
        ],
        'form' => [
            'packagePath' => 'typo3/sysext/form/',
        ],
        'impexp' => [
            'packagePath' => 'typo3/sysext/impexp/',
        ],
        'install' => [
            'packagePath' => 'typo3/sysext/install/',
        ],
        'lowlevel' => [
            'packagePath' => 'typo3/sysext/lowlevel/',
        ],
        'recordlist' => [
            'packagePath' => 'typo3/sysext/recordlist/',
        ],
        'reports' => [
            'packagePath' => 'typo3/sysext/reports/',
        ],
        'scheduler' => [
            'packagePath' => 'typo3/sysext/scheduler/',
        ],
        'sv' => [
            'packagePath' => 'typo3/sysext/sv/',
        ],
        'sys_note' => [
            'packagePath' => 'typo3/sysext/sys_note/',
        ],
        't3editor' => [
            'packagePath' => 'typo3/sysext/t3editor/',
        ],
        'tstemplate' => [
            'packagePath' => 'typo3/sysext/tstemplate/',
        ],
        'viewpage' => [
            'packagePath' => 'typo3/sysext/viewpage/',
        ],
        'hive_cfg_typoscript' => [
            'packagePath' => 'typo3conf/ext/hive_cfg_typoscript/',
        ],
        'hive_thm_bs' => [
            'packagePath' => 'typo3conf/ext/hive_thm_bs/',
        ],
        'hive_thm_custom' => [
            'packagePath' => 'typo3conf/ext/hive_thm_custom/',
        ],
        'hive_honeypot' => [
            'packagePath' => 'typo3conf/ext/hive_honeypot/',
        ],
        'extension_builder' => [
            'packagePath' => 'typo3conf/ext/extension_builder/',
        ],
        'hive_cfg_page' => [
            'packagePath' => 'typo3conf/ext/hive_cfg_page/',
        ],
        'hive_cfg_user' => [
            'packagePath' => 'typo3conf/ext/hive_cfg_user/',
        ],
        'hive_cpt_brand' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_brand/',
        ],
        'hive_cpt_cnt_bs_btn' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_cnt_bs_btn/',
        ],
        'hive_cpt_cnt_bs_carousel' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_cnt_bs_carousel/',
        ],
        'hive_cpt_cnt_bs_tab_collapse' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_cnt_bs_tab_collapse/',
        ],
        'hive_cpt_cnt_img' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_cnt_img/',
        ],
        'hive_cpt_dynamiccontent' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_dynamiccontent/',
        ],
        'hive_cpt_nav_anchor' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_nav_anchor/',
        ],
        'hive_cpt_nav_breadcrumb' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_nav_breadcrumb/',
        ],
        'hive_cpt_nav_mega' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_nav_mega/',
        ],
        'hive_cpt_nav_mobile' => [
            'packagePath' => 'typo3conf/ext/hive_cpt_nav_mobile/',
        ],
        'hive_hooks' => [
            'packagePath' => 'typo3conf/ext/hive_hooks/',
        ],
        'hive_ovr_fluidstyledcontent' => [
            'packagePath' => 'typo3conf/ext/hive_ovr_fluidstyledcontent/',
        ],
        'hive_ovr_metaseo' => [
            'packagePath' => 'typo3conf/ext/hive_ovr_metaseo/',
        ],
        'hive_ovr_realurl' => [
            'packagePath' => 'typo3conf/ext/hive_ovr_realurl/',
        ],
        'hive_ovr_sourceopt' => [
            'packagePath' => 'typo3conf/ext/hive_ovr_sourceopt/',
        ],
        'hive_thm_backendlayout' => [
            'packagePath' => 'typo3conf/ext/hive_thm_backendlayout/',
        ],
        'hive_thm_blazy' => [
            'packagePath' => 'typo3conf/ext/hive_thm_blazy/',
        ],
        'hive_thm_ie_dinosaurs' => [
            'packagePath' => 'typo3conf/ext/hive_thm_ie_dinosaurs/',
        ],
        'hive_thm_jq' => [
            'packagePath' => 'typo3conf/ext/hive_thm_jq/',
        ],
        'hive_thm_jq_focuspoint' => [
            'packagePath' => 'typo3conf/ext/hive_thm_jq_focuspoint/',
        ],
        'hive_thm_jq_hammer' => [
            'packagePath' => 'typo3conf/ext/hive_thm_jq_hammer/',
        ],
        'hive_thm_less' => [
            'packagePath' => 'typo3conf/ext/hive_thm_less/',
        ],
        'hive_thm_modernizr' => [
            'packagePath' => 'typo3conf/ext/hive_thm_modernizr/',
        ],
        'hive_thm_pace' => [
            'packagePath' => 'typo3conf/ext/hive_thm_pace/',
        ],
        'hive_thm_webfontloader' => [
            'packagePath' => 'typo3conf/ext/hive_thm_webfontloader/',
        ],
        'hive_viewhelpers' => [
            'packagePath' => 'typo3conf/ext/hive_viewhelpers/',
        ],
        'metaseo' => [
            'packagePath' => 'typo3conf/ext/metaseo/',
        ],
        'news' => [
            'packagePath' => 'typo3conf/ext/news/',
        ],
        'powermail' => [
            'packagePath' => 'typo3conf/ext/powermail/',
        ],
        'realurl' => [
            'packagePath' => 'typo3conf/ext/realurl/',
        ],
        'sourceopt' => [
            'packagePath' => 'typo3conf/ext/sourceopt/',
        ],
        'vhs' => [
            'packagePath' => 'typo3conf/ext/vhs/',
        ],
    ],
    'version' => 5,
];
